

class ProxyViewAttr:
    admin_list_display = ['pk', 'content_str', 'type', 'site', 'date_modified', 'is_active']
    admin_list_filter = ['type', 'date_modified']
