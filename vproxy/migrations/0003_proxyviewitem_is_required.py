# Generated by Django 2.2.2 on 2020-02-03 08:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vproxy', '0002_auto_20200203_0327'),
    ]

    operations = [
        migrations.AddField(
            model_name='proxyviewitem',
            name='is_required',
            field=models.BooleanField(default=False),
        ),
    ]
