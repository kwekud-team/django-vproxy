# Generated by Django 2.2.2 on 2020-02-03 08:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vproxy', '0003_proxyviewitem_is_required'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProxyViewField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('section', models.CharField(max_length=50)),
                ('field_name', models.CharField(max_length=100)),
                ('label', models.CharField(max_length=250)),
                ('is_active', models.BooleanField(default=True)),
                ('is_required', models.NullBooleanField()),
                ('help_text', models.TextField(blank=True, max_length=250, null=True)),
                ('proxy_view', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='vproxy.ProxyView')),
            ],
            options={
                'ordering': ('section', 'field_name'),
                'unique_together': {('proxy_view', 'field_name', 'section')},
            },
        ),
        migrations.DeleteModel(
            name='ProxyViewItem',
        ),
    ]
