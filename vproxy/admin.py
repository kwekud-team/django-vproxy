from django.contrib import admin

from vproxy.models import ProxyView, ProxyViewField
from vproxy.attrs import ProxyViewAttr
from vproxy.forms import ProxyViewAdminForm, ProxyViewItemAdminForm
from vproxy.utils import VProxyUtils


class ProxyViewFieldInline(admin.TabularInline):
    model = ProxyViewField
    extra = 1
    form = ProxyViewItemAdminForm


@admin.register(ProxyView)
class ProxyViewAdmin(admin.ModelAdmin):
    list_display = ProxyViewAttr.admin_list_display
    list_filter = ProxyViewAttr.admin_list_filter
    form = ProxyViewAdminForm
    search_fields = ['content_str', 'content_pk']
    list_editable = ['is_active']
    inlines = [ProxyViewFieldInline]


class VProxyAdmin(admin.ModelAdmin):

    def get_list_display(self, request):
        list_display = super(VProxyAdmin, self).get_list_display(request)

        vproxy_utils = VProxyUtils(request)

        qs = vproxy_utils.get_qs(self.model)

        list_display = list(list_display)
        for x in qs:
            if x.field_name in list_display:
                pos = list_display.index(x.field_name)

                nm = 'vproxy_%s' % x.field_name
                list_display[pos] = nm

                setattr(self, nm, vproxy_utils.create_attr_callable(x.field_name))
                setattr(getattr(self, nm), 'short_description', x.label)

        return list_display
