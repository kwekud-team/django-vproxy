from django.core.exceptions import FieldDoesNotExist

from kommons.utils.http import get_request_site
from kommons.utils.model import get_str_from_model
from vproxy.models import ProxyViewField
from vproxy.data_classes import VProxyFieldDataC


class VProxyUtils:

    def __init__(self, request):
        self.request = request

    def get_site(self):
        return get_request_site(self.request)

    def get_qs(self, model_class):
        site = self.get_site()
        app_model = get_str_from_model(model_class)
        return ProxyViewField.objects.filter(is_active=True,
                                             proxy_view__site=site,
                                             proxy_view__content_str=app_model)

    def get_vproxy_dict(self, model_class):
        qs = self.get_qs(model_class).values('field_name', 'label', 'is_required', 'help_text')

        return {x['field_name']: VProxyFieldDataC(
                is_proxy=True,
                field_name=x['field_name'],
                is_required=x['is_required'],
                label=x['label'],
                help_text=x['help_text']
            ) for x in qs}

    def get_vproxy_fields(self, model_class, raw_fields, vproxy_dict=None):
        dt = self.get_vproxy_dict(model_class)
        vproxy_dict = vproxy_dict or {}
        dt.update(vproxy_dict)

        xs = []
        for field_name in raw_fields:
            val = None
            new_model_class = model_class
            field_segments = field_name.split('__')
            parent_label = ''

            for pos, new_field_name in enumerate(field_segments):
                if pos < len(field_segments) - 1:
                    remote_field = new_model_class._meta.get_field(new_field_name)
                    new_model_class = remote_field.related_model
                    parent_label += str(remote_field.verbose_name) + ' '
                else:
                    # Use instance of model so any customizations take effect
                    new_model_class = new_model_class()

                    if new_field_name in dt:
                        val = dt[new_field_name]
                        if isinstance(val, str):
                            val = VProxyFieldDataC(
                                field_name=new_field_name,
                                label=val,
                            )
                    else:
                        try:
                            label = new_model_class._meta.get_field(new_field_name).verbose_name
                            val = VProxyFieldDataC(
                                field_name=new_field_name,
                                label=f'{parent_label}{label}',
                            )
                        except FieldDoesNotExist:
                            attr = getattr(new_model_class, new_field_name, None)
                            if not attr:
                                raise AttributeError(f'Field name ({new_field_name}) does not exists in model '
                                                     f'({new_model_class})')

                            if callable(attr):
                                # For functions
                                label = getattr(attr, 'short_description', new_field_name.split('_'))
                            else:
                                # For properties
                                label = new_field_name.split('_')
                            val = VProxyFieldDataC(
                                field_name=new_field_name,
                                label=f'{parent_label}{label}',
                            )
            xs.append(val)
        return xs

    def create_attr_callable(self, field_name):

        class AttrCallable:

            def __init__(self, obj):
                self.obj = obj

            def __str__(self):
                return getattr(self.obj, field_name)

        return AttrCallable
