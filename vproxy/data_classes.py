from dataclasses import dataclass


@dataclass
class VProxyFieldDataC:
    field_name: str
    label: str = ''
    help_text: str = ''
    is_required: bool = None
    is_proxy: bool = False
