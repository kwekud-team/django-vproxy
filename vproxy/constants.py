from enum import Enum


class VProxyK(Enum):
    EMPTY = '---'

    PV_TYPE_DJANGO_MODEL = 'type_django_model'

    PVI_SECTION_ALL = 'section_all'
    PVI_SECTION_DJANGO_FORM = 'section_django_form'
    PVI_SECTION_DJANGO_ADMIN = 'section_django_admin'

    @classmethod
    def create_choices(cls, pattern):
        xs = [('', '---')]
        for key, member in VProxyK.__members__.items():
            if key.startswith(pattern):
                value = member.value
                xs.append((value, value))
        return xs
