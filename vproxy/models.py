from django.db import models

from vproxy.constants import VProxyK


class ProxyView(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    type = models.CharField(max_length=50)
    content_str = models.CharField(max_length=100)
    content_pk = models.CharField(max_length=100, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s: %s' % (self.content_str, self.type)

    class Meta:
        ordering = ('type', 'content_str',)
        unique_together = [('content_str', 'type'), ('content_str', 'content_pk', 'type')]


class ProxyViewField(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    proxy_view = models.ForeignKey(ProxyView, on_delete=models.CASCADE)
    section = models.CharField(max_length=50)
    field_name = models.CharField(max_length=100)
    label = models.CharField(max_length=250)
    is_active = models.BooleanField(default=True)
    is_required = models.BooleanField(null=True)
    help_text = models.TextField(max_length=250, null=True, blank=True)

    def __str__(self):
        return '%s - %s' % (self.section, self.field_name)

    class Meta:
        ordering = ('section', 'field_name',)
        unique_together = ('proxy_view', 'field_name', 'section')

    def save(self, *args, **kwargs):
        self.field_name = self.field_name or VProxyK.EMPTY.value
        super(ProxyViewField, self).save(*args, **kwargs)
