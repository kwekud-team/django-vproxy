from django import forms
from django.contrib.contenttypes.models import ContentType

from kommons.utils.model import get_model_from_str, get_str_from_model
from kommons.utils.http import get_request_site
from vproxy.constants import VProxyK
from vproxy.models import ProxyViewField


class ProxyViewAdminForm(forms.ModelForm):
    type = forms.ChoiceField()
    content_str = forms.ChoiceField()

    def __init__(self, *args, **kwargs):
        super(ProxyViewAdminForm, self).__init__(*args, **kwargs)

        self.fields['type'].choices = VProxyK.create_choices('PV_TYPE_')

        content_str_xs = [(VProxyK.EMPTY.value, '---')]
        if not self.instance.pk:
            content_str_xs = [(VProxyK.EMPTY.value, '---')]
        else:
            if self.instance.type == VProxyK.PV_TYPE_DJANGO_MODEL.value:
                qs = ContentType.objects.order_by('app_label', 'model').values('app_label', 'model')
                content_str_xs = [
                    ('%s.%s' % (x['app_label'], x['model']), '%s.%s' % (x['app_label'], x['model'])) for x in qs]

        self.fields['content_str'].choices = content_str_xs


class ProxyViewItemAdminForm(forms.ModelForm):
    section = forms.ChoiceField()
    field_name = forms.ChoiceField()
    help_text = forms.CharField(required=False, widget=forms.Textarea(attrs={'cols': 40, 'rows': 2}))

    def __init__(self, *args, **kwargs):
        super(ProxyViewItemAdminForm, self).__init__(*args, **kwargs)

        self.fields['section'].choices = VProxyK.create_choices('PVI_SECTION_')
        self.fields['field_name'].required = False

        field_name_xs = [('', '---')]
        if self.instance.pk:
            proxy_view = self.instance.proxy_view
            if proxy_view.type == VProxyK.PV_TYPE_DJANGO_MODEL.value and proxy_view.content_str != VProxyK.EMPTY.value:
                model_class = get_model_from_str(proxy_view.content_str)
                if model_class:
                    fields = model_class._meta.get_fields()
                    # field_name_xs = [('', '---')]
                    field_name_xs.extend([(x.name, x.name) for x in fields])

        self.fields['field_name'].choices = field_name_xs


class VProxyModelForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = getattr(self, 'request', kwargs.pop('request', None))
        super(VProxyModelForm, self).__init__(*args, **kwargs)

        if self.request:
            site = get_request_site(self.request)
            if site:
                app_model = get_str_from_model(self._meta.model)

                qs = ProxyViewField.objects.filter(is_active=True, proxy_view__site=site,
                                                   proxy_view__content_str=app_model)
                for x in qs:
                    if x.field_name in self.fields:
                        if x.label:
                            self.fields[x.field_name].label = x.label
                        if x.help_text:
                            self.fields[x.field_name].help_text = x.help_text
                        if x.is_required is not None:
                            self.fields[x.field_name].required = x.is_required
